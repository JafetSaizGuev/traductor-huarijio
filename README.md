# traductor flutter

Traductor médico de español a Huarijio.
Cuenta con algunas frases y palabras básicas para que el doctor se pueda comunicar con el paciente en su idioma.
La aplicación no requiere el uso de datos para su función por la remota ubicación de los lugares donde se encontrara trabajando el médico.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
