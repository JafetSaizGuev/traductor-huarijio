import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  bool _valorTema = false;
  final _controller = TextEditingController();
  String _filtro = "";

  List<Lista> _Todo = [
    Lista('¿como?','achini'),
    Lista('¿por qué razon?','achinika'),
    Lista('¿como te llamas?','achini tewaniamu'),
    Lista('¿cuando?','achinito'),
    Lista('satisfacerse ','aiwa'),
    Lista('¿ya te llenaste? ','aiwarumupa'),
    Lista('si','ejee'),
    Lista('¿dónde?','akana'),
    Lista('¿donde esta?','akana kahti'),
    Lista('¿a donde vas?','akana simamu'),
    Lista('¿de que sirve?','akanae ahjani'),
    Lista('¿en que lugar?','a kahtia'),
    Lista('rio','aaki'),
    Lista('arroyo','aaki'),
    Lista('jabon','aaoni'),
    Lista('¿quien?','aatana'),
    Lista('¿quieres?','aatana'),
    Lista('mano derecha','ahjamina seeka'),
    Lista('alguna parte','ahka'),
    Lista('en cualquier parte','ahkaoi'),
    Lista('hasta','akasi'),
    Lista('gracias','cheriwema'),
    Lista('calentura','che´e'),
    Lista('¿se enfermo de calentura?','che´epare'),
    Lista('enfermedad con calentura','che´eri'),
    Lista('afectar','che´ewi'),
    Lista('contagiar','che´ewipa'),
    Lista('planta medicinal para hemorragia en las mujeres','chikura'),
    Lista('codo','chitoka'),
    Lista('tener comezon','chihkoke'),
    Lista('tengo mucha comenzon','tisia chihkokorenane'),
    Lista('irritar','chipapa'),
    Lista('irritoso','chipapame'),
    Lista('irritable','chipapame'),
    Lista('amargo','chihpu'),
    Lista('enfermarse de la gripa','choopé'),
    Lista('vas a contagiar de gripa a alguien si tomas agua del mismo vaso','Aaata chuhkemamu choopei, wa ́achi nerojisaa paasochi wahja nerijiachi '),
    Lista('mas','epeché'),
    Lista('higado','eemá'),
    Lista('sangre','eerá'),
    Lista('vena','eerapo'),
    Lista('tener hipo','e´ná'),
    Lista('tengo mucho hipo','tisia enanine'),
    Lista('estornudar','e´túsa'),
    Lista('esta estornudando mucho','tísia enanine'),
    Lista('llaga','ehchá'),
    Lista('ahorita','ehjepa'),
    Lista('regreso pronto','ehjepa no´ramane'),
    Lista('comer','ikopí'),
  ];
  List<Widget> _Mostrar = [];

  void Filtrar(){
    _Mostrar.clear();
    for(int a = 0;a < _Todo.length;a++){
      for(int b = 0;b <= _Todo[a].espanol.length;b++){
        for(int c = 0;c < b;c++){
          String sub = _Todo[a].espanol.substring(c,b);
          if(sub == _filtro) {
            b = _Todo[a].espanol.length;
            _Mostrar.add(dato(_Todo[a].espanol, _Todo[a].otro));
          }
        }
      }
    }
  }

  Container dato(String esp,String otr){
    return Container(child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Expanded(child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 6,horizontal: 14),
          child: Text(esp,),
        )),
        Container(width: 3,height: 15
          ,decoration: BoxDecoration(color: Colors.grey,),),
        Expanded(child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 6,horizontal: 10),
          child: Text(otr),
        )),
      ],
    ),);
  }

  void Completo(){
    _Mostrar.clear();
    for(int a = 0;a < _Todo.length;a++) {
      _Mostrar.add(dato(_Todo[a].espanol, _Todo[a].otro));
    }
  }

  @override
  void initState() {
    super.initState();
    Completo();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: (!_valorTema) ? ThemeData.light() : ThemeData.dark().copyWith(accentColor: Colors.white),
      home: Scaffold(
        appBar: AppBar(
          title: Text('Traductor Español-Huarijio',textAlign: TextAlign.center,),
        ),
        body: SafeArea(
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Expanded(child: Padding(
                    padding: const EdgeInsets.only(left: 20,right: 20),
                    child: TextField(
                      controller: _controller,
                      decoration: InputDecoration(
                          labelText: 'Buscar',
                          prefix: IconButton(icon: Icon(Icons.search), onPressed: (){
                            setState(() {
                              _filtro = _controller.text;
                              Filtrar();
                            });
                          })
                      ),
                    ),
                  )),
                  Row(
                    children: <Widget>[
                      Icon(Icons.wb_sunny),
                      Switch(value: _valorTema, onChanged: (bool val){
                        setState(() {
                          this._valorTema = val;
                        });
                      }),
                      Icon(Icons.brightness_2),
                    ],
                  ),
                ],
              ),
              SizedBox(height: 10),
              Expanded(child: Stack(
                alignment: AlignmentDirectional.topCenter,
                children: <Widget>[
                  ListView.builder(
                    itemCount: _Mostrar.length*2,
                    padding: EdgeInsets.only(top: 15,bottom: 15),
                    itemBuilder: (BuildContext context,int i){
                      if(i % 2 == 0) return Divider();
                      final index = i ~/ 2 ;
                      return _Mostrar[index];
                    },
                  ),
                  Container(decoration: BoxDecoration(
                    color: (!_valorTema) ? Color(0xFF9B9B9B) : Color(0xFF6F706F),
                    borderRadius: BorderRadius.circular(20),
                  ), height: 30,margin: EdgeInsets.symmetric(horizontal: 12),
                    child: Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly,children: <Widget>[
                      Text('Español',style: TextStyle(fontWeight: FontWeight.bold),),IconButton(icon: Icon(Icons.loop),padding: EdgeInsets.only(bottom: 1), onPressed: ()
                      {
                        setState(() {
                          Completo();
                        });
                      }),Text('Huarijio',style: TextStyle(fontWeight: FontWeight.bold),),
                    ],),),
                ],
              )),
            ],
          ),
        ),
      ),
    );
  }
}

class Lista{
  String espanol;
  String otro;

  Lista(this.espanol,this.otro);
}